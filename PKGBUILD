# Maintainer: Alfonso Commer <acommn@gmail.com>

pkgname=emacs-custom
pkgver=29.3
pkgrel=1
pkgdesc="The extensible, customizable, self-documenting, real-time display editor, compiled with custom flags."
arch=('i686'
      'x86_64')
url="http://www.gnu.org/software/emacs/emacs.html"
license=('GPL3')
makedepends=()
depends=('gcc'
         'binutils'
         'libgccjit'
         'gmp'
         'gnutls'
         'gpm'
         'jansson'
         'zlib'
         'tree-sitter'
         'libxpm'
         'librsvg'
         'giflib'
         'imagemagick'
         'hicolor-icon-theme'
         'desktop-file-utils'
         'alsa-lib'
         'm17n-lib'
         'webkit2gtk'
         'sqlite')
provides=('emacs')
conflicts=('emacs'
           'emacs-nox')
source=(${pkgname%-custom}-git::git+https://git.savannah.gnu.org/git/${pkgname%-custom}.git#tag=${pkgname%-custom}-${pkgver})
sha256sums=('SKIP')

build() {
  cd "$srcdir/${pkgname%-custom}-git";
  ./autogen.sh ;
  ./configure \
    --libexecdir=/usr/lib \
    --localstatedir=/var \
    --prefix=/usr \
    --program-transform-name='s/ctags/ctags.emacs/' \
    --sysconfdir=/etc \
    --with-cairo \
    --with-compress-install \
    --with-harfbuzz \
    --with-imagemagick \
    --with-json \
    --with-libgmp \
    --with-mailutils \
    --with-modules \
    --with-native-compilation=aot \
    --with-sound=yes \
    --with-wide-int \
    --with-x-toolkit=lucid \
    --without-pgtk \
    PKG_CONFIG_PATH="/usr/lib/imagemagick6/pkgconfig/" \
    CC="gcc" \
    CFLAGS="$CFLAGS -O3 -mavx2";
  make --silent --no-print-directory -j$(nproc);
}

package() {
  cd "$srcdir/${pkgname%-custom}-git";
  make DESTDIR="$pkgdir" install;
}
